export const populateMantraCreatedBy = () => {
    return {
        path: 'createdBy',
        select: ['firstName', 'lastName', 'username', 'email', 'pic', 'bio']
    }
}