export const populateArticle = () => {
    return [
        {
            path: 'categories',
            select: ['category', 'status']
        },
        {
            path: 'createdBy',
            select: ['firstName', 'lastName', 'username', 'email', 'pic', 'bio']
        },
        {
            path: 'lastUpdatedBy',
            select: ['firstName', 'lastName', 'username', 'email', 'pic', 'bio']
        }
    ];
};