export const populateCommentAuthor = () => {
    return [
        {
            path: 'author',
            select: ['firstName', 'lastName', 'username', 'email', 'pic', 'bio']
        },
        {
            path: 'articleOwner',
            select: ['firstName', 'lastName', 'username', 'email', 'pic', 'bio']
        },
        {
            path: 'commentReplay.author',
            select: ['firstName', 'lastName', 'username', 'email', 'pic', 'bio']
        }
    ];
}