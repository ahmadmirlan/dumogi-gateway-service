import AWS from 'aws-sdk';

const s3 = new AWS.S3();

export async function uploadImageToS3(key, body) {
    const result = await s3.upload({
        Bucket: process.env.MEDITATION_BUCKET_NAME,
        Key: key,
        Body: body,
        ContentEncoding: 'base64',
        ContentType: 'image/jpeg'
    }).promise();

    return result.Location;
}

export async function uploadMusicToS3(key, body) {
    const result = await s3.upload({
        Bucket: process.env.MEDITATION_BUCKET_NAME,
        Key: key,
        Body: body,
        ContentEncoding: 'base64',
        ContentType: 'mp3/mp3'
    }).promise();

    return result.Location;
}
