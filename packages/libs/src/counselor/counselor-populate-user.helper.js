import {selectOnlyGeneralUsersFields} from "../users/user-populate-filed";

export const counselorPopulateUser = () => {
    return {
        path: 'user',
        select: selectOnlyGeneralUsersFields()
    }
}