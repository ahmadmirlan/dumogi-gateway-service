export const selectOnlyGeneralUsersFields = () => {
    return '-password -activationToken -accessToken -isPasswordSet -isAccountActivated -resetPasswordToken';
};

export const populateUserRolesAndPermissions = () => {
    return {path: 'role', populate: {path: 'permissions.permission'}};
};