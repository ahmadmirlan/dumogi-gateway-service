import {
    articleController,
    articleCategoryController,
    meditationController,
    meditationCategoryController,
    mantraController,
    quoteController
} from '@dumogi/controller';
import {auth} from '@dumogi/config';

export const routes = (app) => {
    // Render home page
    app.get('/home', function (req, res) {
        return res.send({message: 'Welcome to Dumogi Media Service'});
    });

    app.get('/version', (req, res) => {
        return res.send({
            service: 'Dumogi Media Service',
            version: 'v0.0.1'
        });
    });

    /*-----------------------Category Routes---------------------------------------*/
    app.get('/meditation/categories/active', meditationCategoryController.findAllActiveCategory);

    /*-----------------------Meditation Routes---------------------------------------*/
    app.get('/meditations/published', [auth], meditationController.getAllPublishedMeditation);
    app.get('/meditation/:id', [auth], meditationController.getMeditationById);
    app.get('/meditation/findByCategory/:meditationCategory', [auth], meditationController.getAllPublishedMeditationByCategory);

    /*-----------------------Article Category Routes---------------------------------------*/
    app.get('/article/categories/active', articleCategoryController.findAllActiveCategory);

    /*-----------------------Article Routes---------------------------------------*/
    app.post('/article/find', articleController.findAllPublishedArticle);
    app.get('/article/find/:id', articleController.findAllPublishedArticleById);
    app.post('/article/related', articleController.findRelatedArticle);

    /*-----------------------Mantra Routes---------------------------------------*/
    app.post('/mantra/find', mantraController.findAllPublishedMantra);
    app.get('/mantra/find/:mantraId', mantraController.findPublishedMantraById);

    /*-----------------------Quote Routes---------------------------------------*/
    app.post('/quote/find', quoteController.findAllPublishedQuote);
    app.get('/quote/find/:quoteId', quoteController.findPublishedQuoteById);
};
