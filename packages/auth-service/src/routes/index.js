import {auth} from '@dumogi/config';
import {authController, userController, permissionController, roleController} from '@dumogi/controller';
import {permissions} from '@dumogi/permissions'

export const routes = (app) => {
    // Render home page
    app.get('/home', function (req, res) {
        return res.send({message: 'Welcome to Dumogi Auth Service'});
    });

    app.get('/version', (req, res) => {
        return res.send({
            service: 'Dumogi Auth Service',
            version: 'v0.0.1'
        });
    });

    // TODO : refactor the authentication should have permissions
    /*-----------------------Role Routes---------------------------------------*/
    app.post('/role', [auth, permissions.canUpdateAdmin], roleController.createNewRole);
    app.delete('/role/:id', [auth, permissions.canDeleteAdmin], roleController.deleteRoleById);
    app.put('/role/:id', [auth, permissions.canUpdateAdmin], roleController.updateRoleById);
    app.get('/role/:id', [auth, permissions.canReadAdmin], roleController.getRoleById);
    app.get('/roles', [auth, permissions.canReadAdmin], roleController.getAllRoles);

    /*-----------------------Permission Routes---------------------------------------*/
    app.post('/permission', [auth, permissions.canUpdateAdmin], permissionController.createNewPermission);
    app.get('/permission/:id', [auth, permissions.canReadAdmin], permissionController.getPermissionById);
    app.delete('/permission/:id', [auth, permissions.canDeleteAdmin], permissionController.deletePermissionById);
    app.put('/permission/:id', [auth, permissions.canUpdateAdmin], permissionController.updatePermission);
    app.get('/permissions', [auth, permissions.canReadAdmin], permissionController.getAllPermissions);

    /*-----------------------Auth Routes---------------------------------------*/
    app.post('/auth/register', authController.register);
    app.post('/auth/login', authController.login);
    app.get('/auth/check/email/:email', authController.isEmailExist);
    app.get('/auth/check/username/:username', authController.isUsernameExist);
    app.post('/auth/change/myPassword', [auth], authController.changeUserPassword);
    app.get('/auth/check/activation', authController.checkActivationToken);
    app.get('/auth/check/registered/activation', authController.checkRegisteredActivationToken);
    app.post('/auth/account/activate', authController.activateUserAccount);
    app.post('/auth/account/activateRegistration', authController.activateRegisteredUserAccount);
    app.get('/auth/request/resetPassword/:email', authController.requestResetPassword);
    app.get('/auth/check/resetPassword', authController.validateResetPasswordToken);
    app.post('/auth/set/resetPassword', authController.setResetPassword);
    app.get('/sendmail/demo', authController.sendMailDemoRequest);


    /*-----------------------User Routes---------------------------------------*/
    app.get('/users', [auth, permissions.canReadAdmin], userController.getAllUser);
    app.get('/user/myInfo', [auth], userController.findUserInfo);
    app.get('/user/:id', [auth, permissions.canReadAdmin], userController.findUserById);
    app.post('/user/add', [auth, permissions.canUpdateAdmin], userController.createNewUser);
    app.put('/user/:id', [auth, permissions.canUpdateAdmin], userController.updateUser);
    app.post('/user/requestUserInfo', [auth], userController.requestUsersInfo);
    app.put('/user/update/myInfo', [auth], userController.updateMyInfo);
    app.get('/user/find/user/:username', userController.findUserByUsername);
    app.get('/user/find/userByEmail/:email', userController.findUserByEmail);
};
