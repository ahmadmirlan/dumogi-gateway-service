import {auth} from '@dumogi/config';
import {articleCommentController, articleCommentReplayController} from '@dumogi/controller';

export const routes = (app) => {
    // Render home page
    app.get('/home', function (req, res) {
        return res.send({message: 'Welcome to Dumogi Comment Service'});
    });

    app.get('/version', (req, res) => {
        return res.send({
            service: 'Dumogi Comment Service',
            version: 'v0.0.1'
        });
    });

    /*-----------------------Article Comments Routes---------------------------------------*/
    app.post('/article/comment/add', [auth], articleCommentController.addNewComment);
    app.get('/article/comment/find/:articleId', articleCommentController.getCommentByArticleId);
    app.put('/article/comment/:commentId/update', [auth], articleCommentController.updateComment);
    app.delete('/article/comment/:commentId/deleted', [auth], articleCommentController.deleteComment);

    /*-----------------------Article Comments Replay Routes---------------------------------------*/
    app.post('/article/replay/comment/add/:parentCommentId', [auth], articleCommentReplayController.addNewReplayComment);
    app.put('/article/replay/comment/edit/:parentCommentId/:replayCommentId', [auth], articleCommentReplayController.editReplayComment);
    app.delete('/article/replay/comment/delete/:parentCommentId/:replayCommentId', [auth], articleCommentReplayController.deleteReplayComment);
};
