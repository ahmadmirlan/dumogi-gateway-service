import {auth} from '@dumogi/config';
import {
    articleController,
    articleCategoryController,
    meditationController,
    meditationCategoryController,
    mantraController,
    quoteController,
    counselingTopicController,
    counselorController
} from '@dumogi/controller';
import {permissions} from '@dumogi/permissions';

export const routes = (app) => {
    // Render home page
    app.get('/home', function (req, res) {
        return res.send({message: 'Welcome to Dumogi Content Service'});
    });

    app.get('/version', (req, res) => {
        return res.send({
            service: 'Dumogi Content Service',
            version: 'v0.0.1'
        });
    });

    /*-----------------------Category Routes---------------------------------------*/
    app.post('/meditation/category', [auth, permissions.canUpdateMeditation], meditationCategoryController.createNewCategory);
    app.put('/meditation/category/:id', [auth, permissions.canUpdateMeditation], meditationCategoryController.updateCategory);
    app.get('/meditation/categories', [auth, permissions.canReadMeditation], meditationCategoryController.findAllCategories);
    app.get('/meditation/categories/active', [auth], meditationCategoryController.findAllActiveCategory);
    app.delete('/meditation/category/:id', [auth, permissions.canDeleteMeditation], meditationCategoryController.deleteCategory);

    /*-----------------------Meditation Routes---------------------------------------*/
    app.post('/meditation', [auth, permissions.canUpdateMeditation], meditationController.createNewMeditation);
    app.get('/meditations', [auth, permissions.canReadMeditation], meditationController.getAllMeditation);
    app.put('/meditation/:id', [auth, permissions.canUpdateMeditation], meditationController.updateMeditation);
    app.delete('/meditation/:id', [auth, permissions.canDeleteMeditation], meditationController.removeMeditation);
    app.get('/meditation/:id', [auth, permissions.canReadMeditation], meditationController.getMeditationById);

    /*-----------------------Article Category Routes---------------------------------------*/
    app.post('/article/category', [auth, permissions.canUpdateArticle], articleCategoryController.createNewCategory);
    app.put('/article/category/:id', [auth, permissions.canUpdateArticle], articleCategoryController.updateCategory);
    app.get('/article/categories', [auth, permissions.canReadArticle], articleCategoryController.findAllCategories);
    app.get('/article/categories/active', articleCategoryController.findAllActiveCategory);
    app.delete('/article/category/:id', [auth, permissions.canDeleteArticle], articleCategoryController.deleteCategory);

    /*-----------------------Article Routes---------------------------------------*/
    app.post('/article/new', [auth, permissions.canUpdateArticle], articleController.createNewArticle);
    app.get('/article/list', [auth, permissions.canReadArticle], articleController.findAllArticle);
    app.get('/article/find/myArticle', [auth, permissions.canReadArticle], articleController.findArticleByUser);
    app.put('/article/update/:id', [auth, permissions.canUpdateArticle], articleController.updateArticle);
    app.delete('/article/delete/:id', [auth, permissions.canDeleteArticle], articleController.removeArticle);
    app.get('/article/find/:id', [auth, permissions.canReadArticle], articleController.getArticleById);

    /*-----------------------Mantra Routes---------------------------------------*/
    app.post('/mantra/new', [auth, permissions.canUpdateMantra], mantraController.createNewMantra);
    app.put('/mantra/update/:mantraId', [auth, permissions.canUpdateMantra], mantraController.updateMantraById);
    app.post('/mantra/find', [auth, permissions.canReadMantra], mantraController.findAllMantra);
    app.get('/mantra/findAll', [auth, permissions.canReadMantra], mantraController.getAllMantraData);
    app.delete('/mantra/delete/:mantraId', [auth, permissions.canDeleteMantra], mantraController.deleteMantraById);

    /*-----------------------Quote Routes---------------------------------------*/
    app.post('/quote/new', [auth, permissions.canUpdateMantra], quoteController.createNewQuote);
    app.put('/quote/update/:quoteId', [auth, permissions.canUpdateMantra], quoteController.updateQuoteById);
    app.post('/quote/find', [auth, permissions.canReadMantra], quoteController.findAllQuote);
    app.get('/quote/findAll', [auth, permissions.canReadMantra], quoteController.getAllQuoteData);
    app.delete('/quote/delete/:quoteId', [auth, permissions.canDeleteMantra], quoteController.deleteQuoteById);

    /*-----------------------Specialist Routes---------------------------------------*/
    app.post('/counseling/topic/new', [auth, permissions.canUpdateCounseling], counselingTopicController.createNewCounselingTopic);
    app.put('/counseling/topic/update/:id', [auth, permissions.canUpdateCounseling], counselingTopicController.updateCounselingTopicById);
    app.get('/counseling/topic/findAll', [auth, permissions.canReadCounseling], counselingTopicController.getAllCounselingTopic);
    app.get('/counseling/topic/find/:id', [auth, permissions.canReadCounseling], counselingTopicController.findCounselingTopicById);
    app.delete('/counseling/topic/delete/:id', [auth, permissions.canDeleteCounseling], counselingTopicController.deleteCounselingTopicById);

    /*-----------------------Specialist Routes---------------------------------------*/
    app.post('/counselor/new', [auth, permissions.canUpdateCounseling], counselorController.createNewCounselor);
    app.put('/counselor/update/:id', [auth, permissions.canUpdateCounseling], counselorController.updateCounselor);
    app.get('/counselor/findAll', [auth, permissions.canReadCounseling], counselorController.getAllCounselor);
    app.delete('/counselor/delete/:id', [auth, permissions.canDeleteCounseling], counselorController.deleteCounselorById);
    app.get('/counselor/find/:id', [auth, permissions.canReadCounseling], counselorController.findCounselorById);
};
