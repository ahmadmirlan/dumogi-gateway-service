import {Schema, model, ObjectId} from 'mongoose';
import toJson from '@meanie/mongoose-to-json';

const mantraSchema = new Schema({
        mantra: {
            type: String,
            required: true,
        },
        status: {
            type: String,
            enum: ['PUBLISHED', 'DELETED', 'DRAFT'],
            default: 'PUBLISHED'
        },
        createdBy: {
            type: ObjectId,
            ref: 'User'
        },
        cover: {
            type: String,
            required: true
        },
    },
    {
        timestamps: true,
        toJSON: {getters: true},
        toObject: {getters: true},
    });

mantraSchema.plugin(toJson);
export const Mantra = model('Mantra', mantraSchema);
