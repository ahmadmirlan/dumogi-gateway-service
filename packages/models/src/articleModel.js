import {Schema, model, ObjectId} from 'mongoose';

const toJson = require('@meanie/mongoose-to-json');

const updatedListSchema = new Schema({
    userId: {
        type: ObjectId,
        ref: 'User'
    }
}, {
    _id: false,
    timestamps: true
});

const postSchema = new Schema({
    title: {
        type: String,
        required: true,
        minlength: 2
    },
    description: {
        type: String,
    },
    categories: [{
        type: ObjectId,
        ref: 'ArticleCategory'
    }],
    cover: {
        type: String
    },
    status: {
        type: String,
        enum: ['PUBLISHED', 'DRAFT'],
        default: 'DRAFT'
    },
    createdBy: {
        type: ObjectId,
        ref: 'User'
    },
    updatedListBy: [updatedListSchema],
    lastUpdatedBy: {
        type: ObjectId,
        ref: 'User'
    }
}, {
    timestamps: true,
    toJSON: {getters: true},
    toObject: {getters: true},
});

postSchema.plugin(toJson);

export const Article = model('Article', postSchema);
