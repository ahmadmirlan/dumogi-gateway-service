import {Schema, model, ObjectId} from 'mongoose';

const toJson = require('@meanie/mongoose-to-json');

const commentReplaySchema = new Schema({
    description: {
        type: String,
        required: true
    },
    status: {
        type: String,
        enum: ['ACTIVE', 'DISABLED', 'DELETED'],
        default: 'ACTIVE',
        required: true
    },
    author: {
        type: ObjectId,
        ref: 'User',
        required: true
    },
    isEdited: {
        type: Boolean,
        default: false
    },
}, {
    timestamps: true,
    toJSON: {getters: true},
    toObject: {getters: true},
});

const commentSchema = new Schema({
    description: {
        type: String,
        required: true
    },
    status: {
        type: String,
        enum: ['ACTIVE', 'DISABLED', 'DELETED'],
        default: 'ACTIVE',
        required: true
    },
    article: {
        type: ObjectId,
        ref: 'Article',
        required: true
    },
    author: {
        type: ObjectId,
        ref: 'User',
        required: true
    },
    articleOwner: {
        type: ObjectId,
        ref: 'User',
        required: true
    },
    isEdited: {
        type: Boolean,
        default: false
    },
    commentReplay: [commentReplaySchema]
}, {
    timestamps: true,
    toJSON: {getters: true},
    toObject: {getters: true},
});

commentSchema.plugin(toJson);

export const ArticleComment = model('ArticleComment', commentSchema);
export const ArticleCommentReplay = model('ArticleCommentReplay', commentReplaySchema);
