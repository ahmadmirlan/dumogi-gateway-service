import {Schema, model, ObjectId} from 'mongoose';
import toJson from '@meanie/mongoose-to-json';

const notificationSchema = new Schema({
        title: {
            type: String,
            required: true
        },
        messages: {
            type: String
        },
        isRead: {
            type: Boolean,
            default: false
        },
        status: {
            type: String,
            enum: ['PUBLISHED', 'DELETED'],
            default: 'PUBLISHED'
        },
        relatedUser: [{
            type: ObjectId,
            ref: 'User'
        }],
        toUser: {
            type: ObjectId,
            ref: 'User',
            required: true
        },
        type: {
            type: String,
            enum: ['AUTH', 'ARTICLE', 'COMMENT']
        }
    },
    {
        timestamps: true,
        toJSON: {getters: true},
        toObject: {getters: true},
    });

notificationSchema.plugin(toJson);
export const Notification = model('Notification', notificationSchema);
