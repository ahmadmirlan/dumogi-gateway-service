import {Schema, model, ObjectId} from 'mongoose';
import toJson from '@meanie/mongoose-to-json';

const quoteSchema = new Schema({
        quote: {
            type: String,
            required: true,
        },
        quotedBy: {
            type: String,
            required: true
        },
        status: {
            type: String,
            enum: ['PUBLISHED', 'DELETED', 'DRAFT'],
            default: 'PUBLISHED'
        },
        createdBy: {
            type: ObjectId,
            ref: 'User'
        }
    },
    {
        timestamps: true,
        toJSON: {getters: true},
        toObject: {getters: true},
    });

quoteSchema.plugin(toJson);
export const Quote = model('Quote', quoteSchema);
