import {Schema, model, ObjectId} from 'mongoose';

const toJson = require('@meanie/mongoose-to-json');

const mediaSchema = new Schema({
    mediaUrl: {
        type: String,
        required: true,
    },
    title: {
        type: String,
        required: true
    },
    duration: {
        type: Number,
        required: true,
        default: 0.0
    },
    type: {
        type: String,
        enum: ['MP3', 'VIDEO'],
        default: 'MP3'
    }
}, {
    _id: false,
    timestamps: true,
});

const meditationSchema = new Schema({
    title: {
        type: String,
        required: true,
        minlength: 2
    },
    description: {
        type: String,
    },
    categories: [{
        type: ObjectId,
        ref: 'MeditationCategory'
    }],
    cover: {
        type: String
    },
    media: [mediaSchema],
    status: {
        type: String,
        enum: ['PUBLISHED', 'DRAFT'],
        default: 'DRAFT'
    },
    meditationType: {
        type: String,
        enum: ['SERIES', 'INSTANT'],
        default: 'SERIES'
    },
    createdBy: {
        type: ObjectId
    },
    lastUpdatedBy: {
        type: ObjectId
    }
}, {
    timestamps: true,
    toJSON: {getters: true},
    toObject: {getters: true},
});

meditationSchema.plugin(toJson);

export const Meditation = model('Meditation', meditationSchema);
