import {Schema, model} from 'mongoose';

const toJson = require('@meanie/mongoose-to-json');

const categorySchema = new Schema({
    category: {
        type: String,
        required: true
    },
    status: {
        type: String,
        enum: ['ACTIVE', 'DISABLED'],
        default: 'ACTIVE',
        required: true
    }
}, {
    timestamps: true,
    toJSON: {getters: true},
    toObject: {getters: true},
});

categorySchema.plugin(toJson);

export const ArticleCategory = model('ArticleCategory', categorySchema);
