import {Schema, model} from 'mongoose';
import toJson from '@meanie/mongoose-to-json';

const permissionRules = new Schema({
    name: {
        type: String,
        required: true,
        minLength: 2,
    },
    title: {
        type: String,
    },
    ruleSelector: {
        type: String,
        required: true,
        minlength: 1
    }
}, {
    _id: false
});

const permissionSchema = new Schema({
        name: {
            type: String,
            required: true,
            minLength: 2,
            unique: true
        },
        title: {
            type: String,
            unique: true
        },
        isCorePermission: {
            type: Boolean,
            required: true,
            default: false
        },
        rules: [permissionRules]
    },
    {
        timestamps: true,
        toJSON: {getters: true},
        toObject: {getters: true},
    });

permissionSchema.plugin(toJson);
export const Permission = model('Permission', permissionSchema);
