import {Schema, model} from 'mongoose';

const toJson = require('@meanie/mongoose-to-json');

const colourSchema = new Schema({
    bgClass: {
        type: String,
        required: true,
        default: 'bg-green'
    },
    textClass: {
        type: String,
        required: true,
        default: 'text-green-contrast'
    },
    name: {
        type: String,
        required: true,
        default: 'Green'
    }
}, {
    id: false
});

const categorySchema = new Schema({
    category: {
        type: String,
        required: true
    },
    status: {
        type: String,
        enum: ['ACTIVE', 'DISABLED'],
        default: 'ACTIVE',
        required: true
    },
    colour: colourSchema,
    cover: {
        type: String,
    }
}, {
    timestamps: true,
    toJSON: {getters: true},
    toObject: {getters: true},
});

categorySchema.plugin(toJson);

export const MeditationCategory = model('MeditationCategory', categorySchema);
