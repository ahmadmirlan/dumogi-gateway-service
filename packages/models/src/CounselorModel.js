import {Schema, model, ObjectId} from 'mongoose';
import toJson from '@meanie/mongoose-to-json';

const counselorSchema = new Schema({
        fullName: {
            type: String,
            required: true,
        },
        picture: {
            type: String,
            required: true
        },
        status: {
            type: String,
            enum: ['ACTIVE', 'DISABLE'],
            default: 'ACTIVE'
        },
        specialist: [{
            type: ObjectId,
            ref: 'CounselingTopic'
        }],
        type: {
            type: String,
            enum: ['COUNSELOR', 'PSYCHOLOGIST']
        },
        user: {
            type: ObjectId,
            ref: 'User'
        },
        gender: {
            type: String,
            enum: ['MALE', 'FEMALE', 'NOT_SET'],
            default: ['NOT_SET']
        },
        bornDate: {
            type: String,
        },
        bio: {
            type: String,
        },
    },
    {
        timestamps: true,
        toJSON: {getters: true},
        toObject: {getters: true},
    });

counselorSchema.plugin(toJson);
export const Counselor = model('Counselor', counselorSchema);
