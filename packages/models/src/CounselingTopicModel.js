import {Schema, model} from 'mongoose';
import toJson from '@meanie/mongoose-to-json';

const colourSchema = new Schema({
    bgClass: {
        type: String,
        required: true,
        default: 'bg-green'
    },
    textClass: {
        type: String,
        required: true,
        default: 'text-green-contrast'
    },
    name: {
        type: String,
        required: true,
        default: 'Green'
    }
}, {
    id: false
});

const counselingTopicSchema = new Schema({
        topic: {
            type: String,
            required: true,
        },
        colour: colourSchema,
        description: {
            type: String,
        },
        status: {
            type: String,
            enum: ['PUBLISHED', 'DELETED', 'DRAFT'],
            default: 'PUBLISHED'
        }
    },
    {
        timestamps: true,
        toJSON: {getters: true},
        toObject: {getters: true},
    });

counselingTopicSchema.plugin(toJson);
export const CounselingTopic = model('CounselingTopic', counselingTopicSchema);
