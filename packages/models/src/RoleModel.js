import {Schema, model, ObjectId} from 'mongoose';

const toJson = require('@meanie/mongoose-to-json');

const permission = new Schema({
    permission: {
        type: ObjectId,
        ref: 'Permission',
    },
    ruleSelected: [{
        type: String
    }]
}, {
    _id: false
});

const roleSchema = new Schema(
    {
        title: {
            type: String,
            unique: true,
            required: true
        },
        isCoreRole: {
            type: Boolean,
            required: true,
            default: false
        },
        isAdministrator: {
            type: Boolean,
            default: false
        },
        permissions: [permission]
    },
    {
        timestamps: true,
        toJSON: {getters: true},
        toObject: {getters: true},
    },
);

roleSchema.plugin(toJson);
export const Role = model('Role', roleSchema);
