export {
    newUserRegisteredTemplate, resetPasswordTemplate, registerAndSendActivationLink
} from "./src/authentication.template";
