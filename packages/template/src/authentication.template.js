export const newUserRegisteredTemplate = (option) => {
    return `
    <p>Hello, ${option.receipentName}</p>
    <div>
        <p>You just invited by ${option.senderName} to join Dumogi!</p>
        <p>A pleasure if you accpet the invitation and we cannot wait to see you to help us spreading the Love and Hope with Dumogi Platform</p>
        <p>To start using Dumogi Platform please click link below to activating your account and provide the password</p>
        <p><a href="${option.activationLink}">Click Here</a> To Activate The Account</p>
        <br/>
        <p>If you unable click the link just follow the link below</p>
        <a href="${option.activationLink}">${option.activationLink}"</a>
        <p>The activatin link will expired in 1x24 Hours</p>
    </div> 
    <div>
        <p>Thank you,</p>
        <p>Best Regards,</p>
        <p>Dumogi Team</p>
    </div>
    `;
};

export const resetPasswordTemplate = (option) => {
    return `
    <p>Hello, ${option.receipentName}</p>
    <div>
        <p>You just request resetting the password!</p>
        <p>If you don't do this action just ignore this email</p>
        <p>Be carefully to keep your account safe!</p>
        <p><a href="${option.resetPasswordLink}">Click Here</a> To Reset Your Password</p>
        <br/>
        <p>If you unable click the link just follow the link below</p>
        <a href="${option.resetPasswordLink}">${option.resetPasswordLink}"</a>
        <p>The reset password link will expired in 1x24 Hours</p>
    </div> 
    <div>
        <p>Thank you,</p>
        <p>Best Regards,</p>
        <p>Dumogi Team</p>
    </div>
    `;
};

export const registerAndSendActivationLink = (option) => {
    return `
    <p>Hello, ${option.receipentName}</p>
    <div>
        <p>Welcome To Dumogi Platform!</p>
        <p>Just one more step to complete your activation account!</p>
        <p>To start using Dumogi Platform please click link below to activating your account!</p>
        <p><a href="${option.activationLink}">Click Here</a> To Activate The Account</p>
        <br/>
        <p>If you unable click the link just follow the link below</p>
        <a href="${option.activationLink}">${option.activationLink}"</a>
        <p>The activatin link will expired in 1x24 Hours</p>
    </div> 
    <div>
        <p>Thank you,</p>
        <p>Best Regards,</p>
        <p>Dumogi Team</p>
    </div>
    `;
}