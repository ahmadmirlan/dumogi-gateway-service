import {auth} from '@dumogi/config';
import {fileController} from '@dumogi/controller';

export const routes = (app) => {
    // Render home page
    app.get('/home', function (req, res) {
        return res.send({message: 'Welcome to Dumogi File Service Gateway'});
    });

    app.get('/version', (req, res) => {
        return res.send({
            version: 'v0.0.1'
        });
    });

    app.post('/file/upload/getUploadFileUrl', [auth], fileController.getFileUploadUrl);
};
