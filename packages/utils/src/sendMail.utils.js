import {sendMail} from "@dumogi/config";
import {newUserRegisteredTemplate, resetPasswordTemplate, registerAndSendActivationLink} from "@dumogi/template";

export const sendSetupAccountMail = async (user, sender) => {
    try {
        const mailSent = await sendMail({
            senderName: 'Dumogi Platform',
            senderMail: sender.email,
            receiptMail: user.email,
            subject: 'Welcome To Dumogi Platform!',
            text: newUserRegisteredTemplate({
                senderName: `${sender.firstName} ${sender.lastName}`,
                receipentName: `${user.firstName} ${user.lastName}`,
                activationLink: 'http://localhost:4200/auth/activation?token=' + user.activationToken + '&email=' + user.email
            })
        });
        if (mailSent) {
            return mailSent;
        }
    } catch (e) {
        return e;
    }
};

export const sendResetPasswordMail = async (user) => {
    try {
        const mailSent = await sendMail({
            senderName: 'Dumogi Platform',
            senderMail: 'noreplay@dispostable.com',
            receiptMail: user.email,
            subject: 'Dumogi - Reset Password Requested!',
            text: resetPasswordTemplate({
                receipentName: `${user.firstName} ${user.lastName}`,
                resetPasswordLink: 'http://localhost:4200/auth/reset/password?token=' + user.resetPasswordToken + '&email=' + user.email
            })
        });
        if (mailSent) {
            return mailSent;
        }
    } catch (e) {
        return e;
    }
};

export const sendRegisteredAndActivationMail = async (user) => {
    try {
        const mailSent = await sendMail({
            senderName: 'Dumogi Platform',
            senderMail: 'register@dispostable.com',
            receiptMail: user.email,
            subject: 'Welcome To Dumogi Platform!',
            text: registerAndSendActivationLink({
                receipentName: `${user.firstName} ${user.lastName}`,
                activationLink: 'http://localhost:4200/auth/activation?token=' + user.activationToken + '&email=' + user.email
            })
        });
        if (mailSent) {
            return mailSent;
        }
    } catch (e) {
        return e;
    }
};