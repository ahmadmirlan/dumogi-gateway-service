import {get} from 'lodash';

export const defineFilterArticleCategoriesQuery = (filter) => {
    if (get(filter, 'categories')) {
        const filterValue = filter.categories;
        filter.categories = {$in: filterValue}
    }
    return filter;
}