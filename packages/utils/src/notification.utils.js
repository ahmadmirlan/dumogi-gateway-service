import {Notification} from '@dumogi/models';

export const createNewNotification = async (notification) => {
    try {
        let notification = new Notification({...notification});
        notification = await notification.save();
        return notification;
    } catch (e) {
        return e;
    }
}