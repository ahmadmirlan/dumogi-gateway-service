import {Role} from "@dumogi/models";

export const canReadAdmin = async (req, res, next) => {
    try {
        const role = await checkRoleHasAccess(req.user.role, 'Admin', 'can_read');
        if (role) {
            return next();
        }
        return res.status(403).sendError('Access denied. user has no permission access');
    } catch (e) {
        return res.status(403).sendError('Access denied. user has no permission access');
    }
}

export const canUpdateAdmin = async (req, res, next) => {
    try {
        const role = await checkRoleHasAccess(req.user.role, 'Admin', 'can_update');
        if (role) {
            return next();
        }
        return res.status(403).sendError('Access denied. user has no permission access');
    } catch (e) {
        return res.status(403).sendError('Access denied. user has no permission access');
    }
}

export const canDeleteAdmin = async (req, res, next) => {
    try {
        const role = await checkRoleHasAccess(req.user.role, 'Admin', 'can_delete');
        if (role) {
            return next();
        }
        return res.status(403).sendError('Access denied. user has no permission access');
    } catch (e) {
        return res.status(403).sendError('Access denied. user has no permission access');
    }
}

export const canReadArticle = async (req, res, next) => {
    try {
        const role = await checkRoleHasAccess(req.user.role, 'Article', 'can_read');
        if (role) {
            return next();
        }
        return res.status(403).sendError('Access denied. user has no permission access');
    } catch (e) {
        return res.status(403).sendError('Access denied. user has no permission access');
    }
}

export const canUpdateArticle = async (req, res, next) => {
    try {
        const role = await checkRoleHasAccess(req.user.role, 'Article', 'can_update');
        if (role) {
            return next();
        }
        return res.status(403).sendError('Access denied. user has no permission access');
    } catch (e) {
        return res.status(403).sendError('Access denied. user has no permission access');
    }
}

export const canDeleteArticle = async (req, res, next) => {
    try {
        const role = await checkRoleHasAccess(req.user.role, 'Article', 'can_delete');
        if (role) {
            return next();
        }
        return res.status(403).sendError('Access denied. user has no permission access');
    } catch (e) {
        return res.status(403).sendError('Access denied. user has no permission access');
    }
}

export const canReadMeditation = async (req, res, next) => {
    try {
        const role = await checkRoleHasAccess(req.user.role, 'Meditation', 'can_read');
        if (role) {
            return next();
        }
        return res.status(403).sendError('Access denied. user has no permission access');
    } catch (e) {
        return res.status(403).sendError('Access denied. user has no permission access');
    }
}

export const canUpdateMeditation = async (req, res, next) => {
    try {
        const role = await checkRoleHasAccess(req.user.role, 'Meditation', 'can_update');
        if (role) {
            return next();
        }
        return res.status(403).sendError('Access denied. user has no permission access');
    } catch (e) {
        return res.status(403).sendError('Access denied. user has no permission access');
    }
}

export const canDeleteMeditation = async (req, res, next) => {
    try {
        const role = await checkRoleHasAccess(req.user.role, 'Meditation', 'can_delete');
        if (role) {
            return next();
        }
        return res.status(403).sendError('Access denied. user has no permission access');
    } catch (e) {
        return res.status(403).sendError('Access denied. user has no permission access');
    }
}

export const canReadMantra = async (req, res, next) => {
    try {
        const role = await checkRoleHasAccess(req.user.role, 'Mantra', 'can_read');
        if (role) {
            return next();
        }
        return res.status(403).sendError('Access denied. user has no permission access');
    } catch (e) {
        return res.status(403).sendError('Access denied. user has no permission access');
    }
}

export const canUpdateMantra = async (req, res, next) => {
    try {
        const role = await checkRoleHasAccess(req.user.role, 'Mantra', 'can_update');
        if (role) {
            return next();
        }
        return res.status(403).sendError('Access denied. user has no permission access');
    } catch (e) {
        return res.status(403).sendError('Access denied. user has no permission access');
    }
}

export const canDeleteMantra = async (req, res, next) => {
    try {
        const role = await checkRoleHasAccess(req.user.role, 'Mantra', 'can_delete');
        if (role) {
            return next();
        }
        return res.status(403).sendError('Access denied. user has no permission access');
    } catch (e) {
        return res.status(403).sendError('Access denied. user has no permission access');
    }
}

export const canReadCounseling = async (req, res, next) => {
    try {
        const role = await checkRoleHasAccess(req.user.role, 'Counseling', 'can_read');
        if (role) {
            return next();
        }
        return res.status(403).sendError('Access denied. user has no permission access');
    } catch (e) {
        return res.status(403).sendError('Access denied. user has no permission access');
    }
}

export const canUpdateCounseling = async (req, res, next) => {
    try {
        const role = await checkRoleHasAccess(req.user.role, 'Counseling', 'can_update');
        if (role) {
            return next();
        }
        return res.status(403).sendError('Access denied. user has no permission access');
    } catch (e) {
        return res.status(403).sendError('Access denied. user has no permission access');
    }
}

export const canDeleteCounseling = async (req, res, next) => {
    try {
        const role = await checkRoleHasAccess(req.user.role, 'Counseling', 'can_delete');
        if (role) {
            return next();
        }
        return res.status(403).sendError('Access denied. user has no permission access');
    } catch (e) {
        return res.status(403).sendError('Access denied. user has no permission access');
    }
}

async function checkRoleHasAccess(roleId, permission, ruleSelected) {
    try {
        const role = await Role.findById(roleId).populate('permissions.permission');
        if (!role || !role.permissions) {
            return false;
        }

        const canReadAdmin = role.permissions.find(perm => {
            return perm.permission && perm.permission.name === permission && perm.ruleSelected.includes(ruleSelected);
        });

        return !!canReadAdmin;
    } catch (e) {
        return false;
    }
}
