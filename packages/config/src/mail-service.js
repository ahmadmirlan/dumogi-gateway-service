const mailgun = require("mailgun-js");

export const sendMail = async (option) => {
    const DOMAIN = process.env.MAIL_DOMAIN;
    const mg = mailgun({apiKey: process.env.MAILGUN_TOKEN, domain: DOMAIN});
    const data = {
        from: `${option.senderName} <noreplay${option.senderMail}>`,
        to: `dumogiqa@yopmail.com, dumogiqa@yopmail.com`,
        subject: `${option.subject}`,
        text: `${option.text}`
    };
    try {
        return await mg.messages().send(data);
    } catch (e) {
        return e;
    }
};

export const sendMailDemo = async (option) => {
    const DOMAIN = process.env.MAIL_DOMAIN;
    const mg = mailgun({apiKey: process.env.MAILGUN_TOKEN, domain: DOMAIN});
    const data = {
        from: 'Excited User <me@samples.mailgun.org>',
        to: 'dumogiqa@yopmail.com, YOU@YOUR_DOMAIN_NAME',
        subject: 'Hello',
        text: 'Testing some Mailgun awesomness!'
    };
    mg.messages().send(data, function (error, body) {
        console.log(body);
    });
}