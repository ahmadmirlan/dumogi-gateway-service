import {pick} from 'lodash';
import {Role} from '@dumogi/models';

/*
 * POST
 * Create New Role
 * */
export const createNewRole = async (req, res) => {
    let body = req.body;
    let role = new Role(pick(body, ['title', 'permissions']));
    try {
        role = await role.save();
        role = await Role.findById(role.id).populate('permissions.permission');
        return res.send(role);
    } catch (err) {
        return res.status(400).sendError(err);
    }
};

/*
 * GET
 * Get All Roles
 * */
export const getAllRoles = async (req, res) => {
    Role.find()
        .populate('permissions.permission')
        .then(data => {
            return res.send(data);
        })
        .catch(err => {
            return res.status(500).sendError(err);
        });
};

/*
 * GET
 * Get Role By Id
 * */
export const getRoleById = async (req, res) => {
    let {id} = req.params;
    Role.findById(id)
        .populate('permissions.permission')
        .then(data => {
            if (data) return res.send(data);
            else
                return res.status(404).sendError(`Role with id ${id} not found!`);
        })
        .catch(err => {
            return res.status(500).sendError(err);
        });
};

/*
 * DELETE
 * Delete Role By Id
 * */
export const deleteRoleById = async (req, res) => {
    let roleId = req.params['id'];
    Role.findByIdAndDelete(roleId)
        .then(data => {
            if (data) {
                return res.send({
                    message: `Role with id ${roleId} deleted!`,
                });
            }
            return res.status(404).sendError(`Role with id ${roleId} not found!`);
        })
        .catch(err => {
            res.status(400).sendError(err);
        });
};

/*
 * UPDATE
 * Update Role By Id
 * */
export const updateRoleById = async (req, res) => {
    let roleId = req.params['id'];
    Role.findByIdAndUpdate(roleId, req.body, {new: true})
        .populate('permissions.permission')
        .then(data => {
            if (data) return res.send(data);
            else return res.status(404).sendError(`Role with id ${roleId} not found`);
        })
        .catch(err => {
            return res.status(404).sendError(err);
        });
};
