import {pick} from 'lodash';
import {Article} from "@dumogi/models";
import {populateArticle} from "@dumogi/libs";
import {QueryBuilder} from "@dumogi/config";
import {defineFilterArticleCategoriesQuery} from '@dumogi/utils';

/*
* POST
* Create new Article
* */
export const createNewArticle = async (req, res) => {
    const userId = req.user.id;
    try {
        let article = new Article(pick(req.body, ['title', 'description', 'categories', 'cover', 'status']));
        article.createdBy = userId;
        article.lastUpdatedBy = userId;
        article.updatedListBy = [];
        article.updatedListBy.push({userId: userId});
        article = await article.save();

        if (!article) {
            return res.status(400).sendError('Failed creating article');
        }

        article = await Article.findById(article.id).populate(populateArticle());
        return res.send(article);
    } catch (e) {
        return res.status(500).sendError('Failed creating article, please try again');
    }
};

/*
* GET
* Find all Article
* */
export const findAllArticle = async (req, res) => {
    try {
        /*let query = QueryBuilder(
            pick(req.query, [
                'search',
                'pageNumber',
                'pageSize',
                'searchField',
                'sortField',
                'sortOrder',
            ]),
        );*/
        // const totalElements = await Article.countDocuments();
        const posts = await Article.find().populate(populateArticle());
        res.send(posts);
    } catch (e) {
        return res.status(400).sendError(e);
    }
};

/*
* PUT
* Update Article
* */
export const updateArticle = async (req, res) => {
    const userId = req.user.id;
    const {id} = req.params;
    try {
        let articleData = pick(req.body, ['title', 'description', 'categories', 'cover', 'media', 'status']);
        articleData.lastUpdatedBy = userId;
        const updatedArticle = await Article.findByIdAndUpdate(id, {...articleData}, {new: true}).populate(populateArticle());
        if (!updatedArticle) {
            return res.status(404).sendMessage(`Article with id ${id} not found!`);
        }
        res.send(updatedArticle);
    } catch (e) {
        return res.status(400).sendError(e);
    }
};

/*
* DELETE
* Remove Article data
* */
export const removeArticle = async (req, res) => {
    const {id} = req.params;
    try {
        await Article.findByIdAndDelete(id).then(resp => {
            if (resp) {
                return res.send({messages: `Article with id ${id} deleted`});
            }
            return res.status(404).sendError('Article not found!');
        });
    } catch (e) {
        return res.status(500).sendError(e);
    }
};

/*
* GET
* Get Article By Id
* */
export const getArticleById = async (req, res) => {
    const {id} = req.params;
    try {
        const article = await Article.findById(id).populate(populateArticle());
        if (article) {
            return res.send(article);
        }
        return res.status(404).sendError('Article not found');
    } catch (e) {
        return res.status(500).sendError(e);
    }
};

/*
* GET
* Find all article based user id
* */
export const findArticleByUser = async (req, res) => {
    const {id} = req.user;
    try {
        const posts = await Article.find({createdBy: id}).populate(populateArticle());
        res.send(posts);
    } catch (e) {
        return res.status(400).sendError(e);
    }
};

/*
* GET
* Find all Article
* */
export const findAllPublishedArticle = async (req, res) => {
    try {
        let query = QueryBuilder(
            pick(req.body, [
                'filter',
                'pageNumber',
                'pageSize',
                'sortField',
                'sortOrder',
            ]),
        );
        query.filter = defineFilterArticleCategoriesQuery(query.filter);

        const totalElements = await Article.countDocuments({...query.filter, status: 'PUBLISHED'});
        const posts = await Article.find({...query.filter, status: 'PUBLISHED'})
            .limit(query.pageSize)
            .skip(query.pageSize * query.pageNumber)
            .sort([[query.sortField, query.sortOrder]])
            .populate(populateArticle());
        res.sendData(posts, query.pageSize, totalElements, query.pageNumber);
    } catch (e) {
        return res.status(400).sendError(e);
    }
};

/*
* GET
* Find all Article
* */
export const findAllPublishedArticleById = async (req, res) => {
    const {id} = req.params;
    try {
        const article = await Article.findOne({_id: id, status: 'PUBLISHED'}).populate(populateArticle());
        if (article) {
            return res.send(article);
        }
        return res.status(404).sendError('Article not found');
    } catch (e) {
        return res.status(500).sendError(e);
    }
};

/*
* POST
* FInd related articles
* */
export const findRelatedArticle = async (req, res) => {
    try {
        const {articleId, categories} = req.body;
        const articles = await Article.find({
            _id: {"$ne": articleId},
            categories: {"$in": categories},
            status: 'PUBLISHED'
        }).limit(5);
        return res.send(articles);
    } catch (e) {
        return res.status(500).sendError(e);
    }
}