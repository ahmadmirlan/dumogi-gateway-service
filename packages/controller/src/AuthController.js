import {User} from '@dumogi/models';
import bcrypt from 'bcryptjs';
import {pick} from 'lodash';
import jwt from "jsonwebtoken";
import {sendResetPasswordMail, sendRegisteredAndActivationMail} from "@dumogi/utils";

/*
 * POST
 * Register new user
 * */
export const register = async (req, res) => {
    let body = req.body;

    //Validate user
    let isEmailExist = await User.findOne({email: body.email});
    let isUsernameExist = await User.findOne({username: body.username});
    if (isEmailExist) return res.status(400).send({messages: 'Email already used!'});
    if (isUsernameExist) {
        return res.status(400).send({'messages': 'Username already taken!'});
    }

    let user = new User(
        pick(body, ['email', 'password', 'username', 'firstName', 'lastName']),
    );

    // Set Default Avatar For User Profile
    user.pic = 'https://via.placeholder.com/300';

    //Encrypt The Password Using bcrypt
    const salt = await bcrypt.genSalt(10);
    user.password = await bcrypt.hash(user.password, salt);
    user.isPasswordSet = true;
    user.role = '6083c1edd359de000889ab11';
    user.activationToken = user.generateAuthToken(user);

    //Save User To Database
    let newUser = await user.save();

    await sendRegisteredAndActivationMail(newUser);
    newUser = pick(newUser, ['firstName', 'lastName', 'email', 'username']);

    return res.send(newUser);
};

/*
* POST
* Login user
* */
export const login = async (req, res) => {
    const {email, password} = req.body;

    if (!email || !password) {
        return res.status(400).sendError('Invalid username/email or password');
    }

    try {
        const user = await User.findOne({
            $or: [{username: email}, {email: email}],
            status: 'ACTIVE',
            isPasswordSet: true
        }).populate('role').select('-activationToken');

        if (!user) {
            return res.status(400).sendError('Invalid username/email or password');
        }

        const validPassword = await bcrypt.compare(req.body.password, user.password);
        if (!validPassword) {
            return res.status(400).sendError('Invalid username/email or password!');
        }

        user.accessToken = user.generateAuthToken(user);

        return res.send(user);

        /*user.save()
            .then(resp => {
                delete resp.password;
                delete resp.activationToken;
                res.send({user: resp});
            })
            .catch(err => {
                return res.status(500).sendError(err);
            });*/

    } catch (e) {
        return res.status(500).sendError(e);
    }
};

/*
* GET
* Check Email
* */
export const isEmailExist = async (req, res) => {
    const {email} = req.params;
    try {
        const user = await User.findOne({email});
        if (user) {
            return res.send({status: true});
        } else {
            return res.send({status: false});
        }
    } catch (e) {
        return res.status(500).sendError('Checking email failed');
    }
};

/*
* GET
* Check Username
* */
export const isUsernameExist = async (req, res) => {
    const {username} = req.params;
    try {
        const user = await User.findOne({username});
        if (user) {
            return res.send({status: true});
        } else {
            return res.send({status: false});
        }
    } catch (e) {
        return res.status(500).sendError('Checking username failed');
    }
};

/*
* POST
* Change Password
* */
export const changeUserPassword = async (req, res) => {
    const {id, email} = req.user;
    const {currentPassword, newPassword, confirmPassword} = req.body;

    if (newPassword !== confirmPassword) {
        return res.status(400).sendError('Password confirmation did not match');
    }

    try {
        const user = await User.findOne({_id: id, email: email});
        if (!user) {
            return res.status(400).sendError('User not found');
        }

        const validPassword = await bcrypt.compare(currentPassword, user.password);
        if (!validPassword) {
            return res.status(400).sendError('Invalid username/email or password!');
        }

        //Encrypt The Password Using bcrypt
        const salt = await bcrypt.genSalt(10);
        user.password = await bcrypt.hash(newPassword, salt);

        user.save().then(() => {
            return res.send({message: 'Password changed!!'});
        });
    } catch (e) {
        return res.status(500).sendError(e);
    }
};

/*
* GET
* Check activation token
* */
export const checkActivationToken = async (req, res) => {
    const {token} = req.query;

    if (!token) {
        return res.sendError('Please provide token!');
    }

    try {
        const user = await User.findOne({activationToken: token, isPasswordSet: false, isAccountActivated: false});

        if (!user) {
            return res.status(400).sendError('Activation failed! token not found');
        }

        const parsedToken = jwt.verify(token, process.env.JWT_PRIVATE_KEY);

        return res.send(parsedToken);
    } catch (e) {
        res.status(401).sendError(' Token Invalid or Expired.');
    }
};

/*
* GET
* Check activation token
* */
export const checkRegisteredActivationToken = async (req, res) => {
    const {token} = req.query;

    if (!token) {
        return res.sendError('Please provide token!');
    }

    try {
        const user = await User.findOne({activationToken: token, isPasswordSet: true, isAccountActivated: false});

        if (!user) {
            return res.status(400).sendError('Activation failed! token not found');
        }

        const parsedToken = jwt.verify(token, process.env.JWT_PRIVATE_KEY);

        return res.send(parsedToken);
    } catch (e) {
        res.status(401).sendError(' Token Invalid or Expired.');
    }
};

/*
* POST
* Validate user
* */
export const activateUserAccount = async (req, res) => {
    const {token, password} = req.body;

    try {
        const parsedToken = await jwt.verify(token, process.env.JWT_PRIVATE_KEY);
        const user = await User.findOne({_id: parsedToken.id, isPasswordSet: false, isAccountActivated: false});

        if (!user) {
            return res.status(400).sendError('Activation failed! token not found');
        }

        //Encrypt The Password Using bcrypt
        const salt = await bcrypt.genSalt(10);
        user.password = await bcrypt.hash(password, salt);
        user.isPasswordSet = true;
        user.isAccountActivated = true;
        user.status = 'ACTIVE';

        await user.save();

        return res.send(user);
    } catch (e) {
        return res.status(500).sendError(e);
    }
};

/*
* POST
* Validate registered user
* */
export const activateRegisteredUserAccount = async (req, res) => {
    const {token} = req.body;

    try {
        const parsedToken = await jwt.verify(token, process.env.JWT_PRIVATE_KEY);
        const user = await User.findOne({_id: parsedToken.id, isPasswordSet: true, isAccountActivated: false});

        if (!user) {
            return res.status(400).sendError('Activation failed! token not found');
        }

        //Encrypt The Password Using bcrypt
        const salt = await bcrypt.genSalt(10);
        user.isAccountActivated = true;
        user.status = 'ACTIVE';

        await user.save();

        return res.send(user);
    } catch (e) {
        return res.status(500).sendError(e);
    }
};


/*
* GET
* Request reset password
* */
export const requestResetPassword = async (req, res) => {
    const {email} = req.params;

    try {
        const user = await User.findOne({
            email,
            isPasswordSet: true,
            isAccountActivated: true,
            status: ['ACTIVE', 'PENDING']
        });
        user.resetPasswordToken = user.generateAuthToken(user);
        await user.save();
        await sendResetPasswordMail(user);
        return res.send({message: 'Reset password sent'});
    } catch (e) {
        return res.status(500).sendError('User not found!');
    }
};

/*
* GET
* Validate reset password token
* */
export const validateResetPasswordToken = async (req, res) => {
    const {token} = req.query;

    if (!token) {
        return res.sendError('Please provide token!');
    }

    try {
        const user = await User.findOne({resetPasswordToken: token, isPasswordSet: true, isAccountActivated: true});

        if (!user) {
            return res.status(400).sendError('Activation failed! token not found');
        }

        const parsedToken = await jwt.verify(token, process.env.JWT_PRIVATE_KEY);

        return res.send(parsedToken);
    } catch (e) {
        res.status(401).sendError(' Token Invalid or Expired.');
    }
};

/*
* POST
* Set new password
* */
export const setResetPassword = async (req, res) => {
    const {password, token} = req.body;
    if (!password || !token) {
        return res.status(400).sendError('Request not match!');
    }

    try {
        const parsedToken = await jwt.verify(token, process.env.JWT_PRIVATE_KEY);
        const user = await User.findById(parsedToken.id);

        //Encrypt The Password Using bcrypt
        const salt = await bcrypt.genSalt(10);
        user.password = await bcrypt.hash(password, salt);
        user.resetPasswordToken = null;
        await user.save();

        return res.send({message: 'Password changed!'});
    } catch (e) {
        return res.status(500).sendError(e);
    }
};

export const sendMailDemoRequest = async (req, res) => {
    try {
        const user = await User.findOne();
        await sendRegisteredAndActivationMail(user);
        return res.send({message: 'OK'})
    } catch (e) {
        return res.status(500).sendError(e);
    }
}