import {MeditationCategory} from '@dumogi/models';
import {pick} from 'lodash';

/*
* POST
* Create new category
* */
export const createNewCategory = async (req, res) => {
    const {category, colour, cover} = req.body;

    try {
        let categoryData = new MeditationCategory({category, colour, cover});

        categoryData = await categoryData.save();
        return res.send(categoryData);
    } catch (e) {
        res.status(400).sendError(e);
    }
};

/*
* POST
* Find all category
* */
export const findAllCategories = async (req, res) => {
    try {
        const categories = await MeditationCategory.find();
        res.send(categories);
    } catch (e) {
        return res.status(400).sendError(e);
    }
};

/*
* PUT
* Update category
* */
export const updateCategory = async (req, res) => {
    const {id} = req.params;
    const categoryData = pick(req.body, ['status', 'category', 'colour', 'cover']);

    try {
        let findCategory = await MeditationCategory.findByIdAndUpdate(id, {...categoryData}, {new: true});

        if (!findCategory) {
            return res.status(404).sendMessage(`Category with id ${id} not found!`);
        }
        res.send(findCategory);
    } catch (e) {
        return res.status(400).sendError(e);
    }
};

/*
* DELETE
* Remove category
* */
export const deleteCategory = async (req, res) => {
    const {id} = req.params;
    try {
        const category = await MeditationCategory.findById(id);
        if (!category) {
            return res.status(404).sendError('Category not found!');
        }
        await MeditationCategory.findByIdAndDelete(id);
        return res.send({messages: `Category with id ${id} deleted!`});
    } catch (e) {
        return res.status(500).sendError('Cannot delete category');
    }
};

/*
* GET
* Find all active category
* */
export const findAllActiveCategory = async (req, res) => {
    try {
        const categories = await MeditationCategory.find({status: 'ACTIVE'});
        return res.send(categories);
    } catch (e) {
        return res.status(500).sendError('Failed load active category');
    }
};
