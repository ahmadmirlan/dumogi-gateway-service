import {pick} from 'lodash';
import {ArticleComment, ArticleCommentReplay} from '@dumogi/models';
import {populateCommentAuthor} from '@dumogi/libs';

/*
* POST
* Add new replay comment
* */
export const addNewReplayComment = async (req, res) => {
    try {
        const {parentCommentId} = req.params;
        const commentReplay = new ArticleCommentReplay(pick(req.body, ['description']));
        commentReplay.author = req.user.id;
        const findParentComment = await ArticleComment.findOne({_id: parentCommentId, status: 'ACTIVE'});
        if (!findParentComment) {
            return res.status(404).sendError('Parent comment not found');
        }
        findParentComment.commentReplay.push(commentReplay);
        const newEditedComment = await ArticleComment.findOneAndUpdate({
            _id: parentCommentId,
            status: 'ACTIVE'
        }, findParentComment, {new: true}).populate(populateCommentAuthor());
        return res.send(newEditedComment);
    } catch (e) {
        return res.status(400).sendError(e);
    }
}

/*
* PUT
* Edit Replay Comment
* */
export const editReplayComment = async (req, res) => {
    try {
        const {parentCommentId, replayCommentId} = req.params;
        const {description} = req.body;
        const findParentComment = await ArticleComment.findOne({_id: parentCommentId, status: 'ACTIVE'});
        if (!findParentComment) {
            return res.status(404).sendError('Parent comment not found');
        }
        const replayedComments = findParentComment.commentReplay.map(comm => {
            if (comm._id.toString() === replayCommentId.toString() && comm.author.toString() === req.user.id.toString()) {
                comm.description = description;
            }
            return comm;
        });
        console.log(replayedComments);
        const newUpdatedComment = await ArticleComment.findOneAndUpdate({
            _id: parentCommentId,
            status: 'ACTIVE'
        }, {commentReplay: replayedComments}, {new: true}).populate(populateCommentAuthor());
        return res.send(newUpdatedComment);
    } catch (e) {
        return res.status(400).sendError(e);
    }
}

/*
* DELETE
* Delete Replay Comment
* */
export const deleteReplayComment = async (req, res) => {
    try {
        const {parentCommentId, replayCommentId} = req.params;
        const findParentComment = await ArticleComment.findOne({_id: parentCommentId, status: 'ACTIVE'});
        if (!findParentComment) {
            return res.status(404).sendError('Parent comment not found');
        }
        const replayedComments = findParentComment.commentReplay.filter(comm => {
            return comm.id.toString() !== replayCommentId.toString() && comm.author.toString() === req.user.id.toString();
        });
        const newUpdatedComment = await ArticleComment.findOneAndUpdate({
            _id: parentCommentId,
            status: 'ACTIVE'
        }, {commentReplay: replayedComments}, {new: true}).populate(populateCommentAuthor());
        return res.send(newUpdatedComment);
    } catch (e) {
        return res.status(400).sendError(e);
    }
}
