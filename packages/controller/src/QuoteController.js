import {Quote} from '@dumogi/models';
import {pick} from 'lodash';
import {QueryBuilder} from '@dumogi/config';
import {populateMantraCreatedBy} from '@dumogi/libs';


/*
* POST
* Create New Quote
* */
export const createNewQuote = async (req, res) => {
    try {
        let quote = new Quote(pick(req.body, ['quote', 'quotedBy', 'status']));
        quote.createdBy = req.user.id;
        quote = await quote.save();
        return res.send(quote);
    } catch (e) {
        return res.status(400).sendError(e);
    }
}

/*
* PUT
* Update Quote
* */
export const updateQuoteById = async (req, res) => {
    try {
        const {quoteId} = req.params;
        const quote = await Quote.findOneAndUpdate({_id: quoteId}, req.body, {new: true});
        if (!quote) {
            return res.status(400).sendError('Failed updating mantra');
        }
        return res.send(quote);
    } catch (e) {
        return res.status(400).sendError(e);
    }
}

/*
* GET
* Get all quote data
* */
export const getAllQuoteData = async (req, res) => {
    try {
        const quotes = await Quote.find().populate(populateMantraCreatedBy()).sort([['updatedAt', 'desc']]);
        return res.send(quotes);
    } catch (e) {
        return res.status(400).sendError(e);
    }
}

/*
* DELETE
* Delete quote by id
* */
export const deleteQuoteById = async (req, res) => {
    try {
        const {quoteId} = req.params;
        if (!quoteId) {
            return res.status(400).sendError('Please provide quote id');
        }
        await Quote.findByIdAndDelete(quoteId);
        return res.send({message: 'Quote deleted!'});
    } catch (e) {
        return res.status(400).sendError(e);
    }
}

/*
* POST
* Find All Quote
* */
export const findAllQuote = async (req, res) => {
    try {
        const query = QueryBuilder(
            pick(req.body, [
                'filter',
                'pageNumber',
                'pageSize',
                'sortField',
                'sortOrder',
            ]),
        );

        const totalElements = await Quote.countDocuments({...query.filter});
        const quotes = await Quote.find({...query.filter})
            .limit(query.pageSize)
            .skip(query.pageSize * query.pageNumber)
            .sort([[query.sortField, query.sortOrder]])
            .populate(populateMantraCreatedBy());
        res.sendData(quotes, query.pageSize, totalElements, query.pageNumber);
    } catch (e) {
        return res.status(400).sendError(e);
    }
}

/*
* POST
* Find All Published Quote
* */
export const findAllPublishedQuote = async (req, res) => {
    try {
        const query = QueryBuilder(
            pick(req.body, [
                'filter',
                'pageNumber',
                'pageSize',
                'sortField',
                'sortOrder',
            ]),
        );

        const totalElements = await Quote.countDocuments({...query.filter, status: 'PUBLISHED'});
        const quotes = await Quote.find({...query.filter, status: 'PUBLISHED'})
            .limit(query.pageSize)
            .skip(query.pageSize * query.pageNumber)
            .sort([[query.sortField, query.sortOrder]])
            .populate(populateMantraCreatedBy());
        res.sendData(quotes, query.pageSize, totalElements, query.pageNumber);
    } catch (e) {
        return res.status(500).sendError(e);
    }
}

/*
* GET
* Find Published Quote By Id
* */
export const findPublishedQuoteById = async (req, res) => {
    try {
        const {quoteId} = req.params;
        const quote = await Quote.findById(quoteId).populate(populateMantraCreatedBy());
        if (!quote) {
            return res.status(400).sendError('Quote not found!');
        }
        return res.send(quote);
    } catch (e) {
        return res.status(500).sendError(e);
    }
}