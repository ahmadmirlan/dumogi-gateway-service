import {Notification} from '@dumogi/models';

/*
* POST
* Create Notification
* */
export const getAllMyNotification = async (req, res) => {
    const {user} = req;
    try {
        const notifications = await Notification.find({toUser: user.id, status: 'PUBLISHED'})
            .limit(10);
        return res.send({notifications});
    } catch (e) {
        return res.status(500).sendError('Failed load notification data!');
    }
};
