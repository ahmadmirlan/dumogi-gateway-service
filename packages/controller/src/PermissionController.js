import {pick, remove} from 'lodash';
import {Permission} from '@dumogi/models';
import {Role} from '@dumogi/models';
import {initRulePermission} from "@dumogi/utils";

/*
 * POST
 * Create New Permission
 * */
export const createNewPermission = async (req, res) => {
    try {
        let body = req.body;
        let permission = new Permission(
            pick(body, ['name', 'title']),
        );

        permission.rules = initRulePermission(permission.name);

        permission = await permission.save();
        /*
        * Find administrator role
        * Core role should be named {title: Administrator, isCoreRole: true}
        * */
        let coreRole = await Role.findOne({
            isAdministrator: true,
        });
        if (permission && coreRole) {
            coreRole.permissions.push({
                permission: permission.id,
                ruleSelected: [
                    "can_update",
                    "can_read",
                    "can_delete"
                ]
            });
            await coreRole.save();
        }
        return res.send(permission);
    } catch (e) {
        return res.status(400).sendError(e);
    }
};

/*
 * GET
 * Get Permission By Id
 * */
export const getPermissionById = async (req, res) => {
    let permissionId = req.params['id'];
    Permission.findById(permissionId)
        .then(data => {
            if (data) return res.send(data);
            else
                return res
                    .status(404)
                    .sendError(`Permission with id ${permissionId} not found!`);
        })
        .catch(err => {
            return res.status(500).sendError(err);
        });
};

/*
 * GET
 * Find All Permission
 * */
export const getAllPermissions = async (req, res) => {
    try {
        const permissions = await Permission.find();
        return res.send(permissions);
    } catch (e) {
        return res.status(500).sendError('Failed load permissions data');
    }
};

/*
 * DELETE
 * Delete Permission By Id
 * */
export const deletePermissionById = async (req, res) => {
    const {id} = req.params;
    try {
        let permission = await Permission.findOne({_id: id, isCorePermission: false});

        if (!permission) {
            return res.status(400).sendError('permission not found');
        }

        permission = await permission.delete();
        let roles = await Role.find();
        if (permission) {
            for (let role of roles) {
                remove(role.permissions, _perm => {
                    return _perm.toString() === id.toString();
                });
                await role.save();
            }
        }
        return res.send({message: `Permission with id ${id} deleted!`});
    } catch (e) {
        res.status(400).sendError(e);
    }
};

/*
* PUT
* Update permission
* */
export const updatePermission = async (req, res) => {
    const {id} = req.params;
    try {
        const permission = await Permission.findOneAndUpdate({
            _id: id,
            isCorePermission: false
        }, {...pick(req.body, ['name', 'title']), rules: initRulePermission(req.body.name)}, {new: true});
        return res.send(permission);
    } catch (e) {
        return res.status(500).sendError('Failed updating permissions');
    }
};
