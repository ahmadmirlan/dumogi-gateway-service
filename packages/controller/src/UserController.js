import {pick, pull} from 'lodash';
import {User} from "@dumogi/models";
import {Role} from "@dumogi/models";
import {sendSetupAccountMail} from "@dumogi/utils";
import {populateUserRolesAndPermissions, selectOnlyGeneralUsersFields} from "@dumogi/libs";

/*
* POST
* Find all users
* */
export const getAllUser = async (req, res) => {
    try {
        const users = await User.find().select(selectOnlyGeneralUsersFields())
            .populate(populateUserRolesAndPermissions());
        return res.send(users);
    } catch (e) {
        return res.status(500).sendError('Failed load users data');
    }
};

/*
* GET
* Get user by id
* */
export const findUserById = async (req, res) => {
    const {id} = req.params;

    try {
        const user = await User.findById(id)
            .select(selectOnlyGeneralUsersFields())
            .populate(populateUserRolesAndPermissions());

        if (!user) {
            return res.status(404).sendError('User not found!');
        }
        return res.send(user);
    } catch (e) {
        return res.status(500).sendError('Failed find user');
    }
};

/*
* GET
* Find user info
* */
export const findUserInfo = async (req, res) => {
    const {id} = req.user;

    try {
        const user = await User.findById(id).select(selectOnlyGeneralUsersFields())
            .populate(populateUserRolesAndPermissions());

        if (!user) {
            return res.status(400).sendError('Please re login');
        }
        return res.send(user);
    } catch (e) {
        return res.status(500).sendError('Failed load user info');
    }
};

/*
* POST
* Create new user
* */
export const createNewUser = async (req, res) => {
    const body = req.body;
    const {id} = req.user;

    try {
        const userCreatedBy = await User.findById(id);
        //Validate user
        const isEmailExist = await User.findOne({email: body.email});
        const isUsernameExist = await User.findOne({username: body.username});
        if (isEmailExist) return res.status(400).send({messages: 'Email already used!'});
        if (isUsernameExist) {
            return res.status(400).send({'messages': 'Username already taken!'});
        }

        const user = new User(
            pick(body, ['email', 'role', 'username', 'firstName', 'lastName']),
        );

        /*
        * Check role
        * */
        const role = await Role.findById(user.role);


        // Set Default Avatar For User Profile
        user.pic = 'https://via.placeholder.com/300';

        user.activationToken = user.generateAuthToken(user);
        user.status = 'INACTIVE';

        // Save User To Database
        let newUser = await user.save();
        await sendSetupAccountMail(newUser, userCreatedBy);

        newUser.role = role;
        newUser = pull(newUser, 'password', 'accessToken', 'refreshToken', 'activationToken');

        return res.send(newUser);
    } catch (e) {
        return res.status(500).sendError(e);
    }
};

/*
* PUT
* Update user
* */
export const updateUser = async (req, res) => {
    const userData = pick(req.body, ['firstName', 'lastName', 'username', 'email', 'role', 'status']);
    const {id} = req.params;
    try {
        const user = await User.findByIdAndUpdate(id, {...userData}, {new: true}).populate('role');
        return res.send(pull(user, 'password', 'accessToken', 'refreshToken', 'activationToken'));
    } catch (e) {
        return res.status(500).sendError('Cannot update user');
    }
};

/*
* POST
* Find users by provided id
* */
export const requestUsersInfo = async (req, res) => {
    const {usersId} = req.body;

    if (!usersId) {
        return res.status(400).sendError('Please provided user id');
    }

    try {
        const userData = await User.find({'_id': {$in: usersId}})
            .select(selectOnlyGeneralUsersFields())
            .populate(populateUserRolesAndPermissions());
        return res.send(userData);
    } catch (e) {
        return res.sendError(e);
    }
};

/*
* PUT
* Update My Info
* */
export const updateMyInfo = async (req, res) => {
    const {id} = req.user;
    const userData = pick(req.body, ['firstName', 'lastName', 'pic', 'bio']);

    try {
        const user = await User.findByIdAndUpdate(id, {...userData}, {new: true})
            .select(selectOnlyGeneralUsersFields())
            .populate(populateUserRolesAndPermissions());

        if (!user) {
            return res.status(400).sendError('User not found');
        }

        return res.send(user);
    } catch (e) {
        return res.status(500).sendError(e);
    }
};

/*
* GET
* Find user by username*/
export const findUserByUsername = async (req, res) => {
    try {
        const {username} = req.params;
        const user = await User.findOne({username: username, status: 'ACTIVE'})
            .select(selectOnlyGeneralUsersFields())
            .populate(populateUserRolesAndPermissions());

        if (!user) {
            return res.status(404).sendError('User not found!');
        }
        return res.send(user);
    } catch (e) {
        return res.status(500).sendError(e);
    }
}

/*
* GET
* Find user by email
* */
export const findUserByEmail = async (req, res) => {
    try {
        const {email} = req.params;
        const user = await User.findOne({email: email, status: 'ACTIVE'})
            .select(selectOnlyGeneralUsersFields())
            .populate(populateUserRolesAndPermissions());

        if (!user) {
            return res.status(404).sendError('User not found!');
        }
        return res.send(user);
    } catch (e) {
        return res.status(500).sendError(e);
    }
}