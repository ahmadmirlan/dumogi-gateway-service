import {Mantra} from '@dumogi/models';
import {pick} from 'lodash';
import {QueryBuilder} from '@dumogi/config';
import {populateMantraCreatedBy} from '@dumogi/libs';

/*
* POST
* Create new mantra
* */
export const createNewMantra = async (req, res) => {
    try {
        let mantra = new Mantra(pick(req.body, ['mantra', 'status', 'cover']));
        mantra.createdBy = req.user.id;
        mantra = await mantra.save();
        return res.send(mantra);
    } catch (e) {
        return res.status(400).sendError(e);
    }
}

/*
* PUT
* Update mantra
* */
export const updateMantraById = async (req, res) => {
    try {
        const {mantraId} = req.params;
        const mantra = await Mantra.findOneAndUpdate({_id: mantraId}, req.body, {new: true});
        if (!mantra) {
            return res.status(400).sendError('Failed updating mantra');
        }
        return res.send(mantra);
    } catch (e) {
        return res.status(400).sendError(e);
    }
}

/*
* GET
* Get all mantra data
* */
export const getAllMantraData = async (req, res) => {
    try {
        const mantras = await Mantra.find().populate(populateMantraCreatedBy()).sort([['updatedAt', 'desc']]);
        return res.send(mantras);
    } catch (e) {
        return res.status(400).sendError(e);
    }
}

/*
* DELETE
* Delete mantra by id
* */
export const deleteMantraById = async (req, res) => {
    try {
        const {mantraId} = req.params;
        if (!mantraId) {
            return res.status(400).sendError('Please provide mantra id');
        }
        await Mantra.findByIdAndDelete(mantraId);
        return res.send({message: 'Mantra deleted!'});
    } catch (e) {
        return res.status(400).sendError(e);
    }
}

/*
* POST
* Find All Mantra
* */
export const findAllMantra = async (req, res) => {
    try {
        const query = QueryBuilder(
            pick(req.body, [
                'filter',
                'pageNumber',
                'pageSize',
                'sortField',
                'sortOrder',
            ]),
        );

        const totalElements = await Mantra.countDocuments({...query.filter});
        const mantras = await Mantra.find({...query.filter})
            .limit(query.pageSize)
            .skip(query.pageSize * query.pageNumber)
            .sort([[query.sortField, query.sortOrder]])
            .populate(populateMantraCreatedBy());
        res.sendData(mantras, query.pageSize, totalElements, query.pageNumber);
    } catch (e) {
        return res.status(400).sendError(e);
    }
}

/*
* POST
* Find All Published Mantra
* */
export const findAllPublishedMantra = async (req, res) => {
    try {
        const query = QueryBuilder(
            pick(req.body, [
                'filter',
                'pageNumber',
                'pageSize',
                'sortField',
                'sortOrder',
            ]),
        );

        const totalElements = await Mantra.countDocuments({...query.filter, status: 'PUBLISHED'});
        const mantras = await Mantra.find({...query.filter, status: 'PUBLISHED'})
            .limit(query.pageSize)
            .skip(query.pageSize * query.pageNumber)
            .sort([[query.sortField, query.sortOrder]])
            .populate(populateMantraCreatedBy());
        res.sendData(mantras, query.pageSize, totalElements, query.pageNumber);
    } catch (e) {
        return res.status(500).sendError(e);
    }
}

/*
* GET
* Find Published Mantra By Id
* */
export const findPublishedMantraById = async (req, res) => {
    try {
        const {mantraId} = req.params;
        const mantra = await Mantra.findById(mantraId).populate(populateMantraCreatedBy());
        if (!mantra) {
            return res.status(400).sendError('Mantra not found!');
        }
        return res.send(mantra);
    } catch (e) {
        return res.status(500).sendError(e);
    }
}