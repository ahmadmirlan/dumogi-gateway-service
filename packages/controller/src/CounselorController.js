import {pick} from 'lodash';
import {Counselor} from "@dumogi/models";
import {counselorPopulateUser} from "@dumogi/libs";

/*
* POST
* Create new counselor
* */
export const createNewCounselor = async (req, res) => {
    try {
        let counselor = new Counselor(pick(req.body, ['fullName', 'picture', 'status', 'specialist', 'type', 'user', 'gender', 'bornDate', 'bio']));
        counselor = await counselor.save();
        counselor = await Counselor.findById(counselor.id).populate([counselorPopulateUser(), 'specialist']);
        return res.send(counselor);
    } catch (e) {
        return res.status(500).sendError(e);
    }
}

/*
* PUT
* Update counselor
* */
export const updateCounselor = async (req, res) => {
    const {id} = req.params;
    try {
        const counselorData = pick(req.body, ['fullName', 'picture', 'status', 'specialist', 'type', 'user', 'gender', 'bornDate', 'bio']);
        const counselor = await Counselor.findByIdAndUpdate(id, counselorData, {new: true}).populate([counselorPopulateUser(), 'specialist']);
        if (!counselor) {
            return res.status(400).sendMessage(`Counselor with id ${id} failed to update!`);
        }
        return res.send(counselor);
    } catch (e) {
        return res.status(500).sendError(e);
    }
}

/*
* GET
* Find all counselor
* */
export const getAllCounselor = async (req, res) => {
    try {
        const counselor = await Counselor.find().populate([counselorPopulateUser(), 'specialist']);
        return res.send(counselor);
    } catch (e) {
        return res.status(500).sendError(e);
    }
}

/*
* GET
* Find counselor by id
* */
export const findCounselorById = async (req, res) => {
    const {id} = req.params;
    try {
        const counselor = await Counselor.findById(id).populate([counselorPopulateUser(), 'specialist']);
        return res.send(counselor);
    } catch (e) {
        return res.status(500).sendError(e);
    }
}

/*
* DELETE
* Delete counselor by id
* */
export const deleteCounselorById = async (req, res) => {
    const {id} = req.params;
    try {
        const counselor = await Counselor.findByIdAndDelete(id);
        if (!counselor) {
            return res.status(400).sendError('Failed remove counselor!');
        }
        return res.send({message: 'Counselor deleted!'});
    } catch (e) {
        return res.status(500).sendError(e);
    }
}