import {pick} from 'lodash';
import {ArticleComment} from '@dumogi/models';
import {populateCommentAuthor} from '@dumogi/libs';

/*
* POST
* Add new comment
* */
export const addNewComment = async (req, res) => {
    try {
        let comment = new ArticleComment(pick(req.body, ['article', 'articleOwner', 'description']));
        comment.author = req.user.id;
        comment = await comment.save();
        const newCreatedComment = await ArticleComment.findById(comment.id).populate(populateCommentAuthor());
        return res.send(newCreatedComment);
    } catch (e) {
        return res.status(400).sendError(e);
    }
}

/*
* GET
* Find all comments belongs to article id
* */
export const getCommentByArticleId = async (req, res) => {
    try {
        const {articleId} = req.params;
        const comments = await ArticleComment.find({
            article: articleId,
            status: 'ACTIVE'
        }).populate(populateCommentAuthor());
        return res.send(comments);
    } catch (e) {
        return res.status(400).sendError(e);
    }
}

/*
* PUT
* Update comment
* */
export const updateComment = async (req, res) => {
    const {commentId} = req.params;
    const {description} = req.body;
    try {
        let comment = await ArticleComment.findOne({_id: commentId, author: req.user.id});
        if (!comment) {
            return res.status(400).sendError('Comment not found');
        }
        comment = await ArticleComment.findOneAndUpdate({
            _id: commentId,
            author: req.user.id
        }, {description, isEdited: true}, {new: true}).populate(populateCommentAuthor());

        return res.send(comment);
    } catch (e) {
        return res.status(400).sendError(e);
    }
}

/*
* DELETE
* Delete comment
* */
export const deleteComment = async (req, res) => {
    try {
        const {commentId} = req.params;
        await ArticleComment.findOneAndUpdate({
            _id: commentId,
            author: req.user.id
        }, {status: 'DELETED', isEdited: true}, {new: true}).populate(populateCommentAuthor()).then(comment => {
            return res.send(comment);
        });
    } catch (e) {
        return res.status(400).sendError(e);
    }
}