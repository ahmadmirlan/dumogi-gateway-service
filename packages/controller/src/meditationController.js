import {pick} from 'lodash';
import {Meditation} from "@dumogi/models";

/*
* POST
* Create new meditation
* */
export const createNewMeditation = async (req, res) => {
    const userId = req.user.id;
    try {
        let meditation = new Meditation(pick(req.body, ['title', 'description', 'categories', 'cover', 'media', 'status', 'meditationType']));
        meditation.createdBy = userId;
        meditation.lastUpdatedBy = userId;
        meditation = await meditation.save();

        if (!meditation) {
            return res.status(400).sendError('Failed creating meditation');
        }

        meditation = await Meditation.findById(meditation.id).populate({
            path: 'categories',
            select: ['category', 'colour', 'status']
        });
        return res.send(meditation);
    } catch (e) {
        return res.status(500).sendError('Failed creating meditation, please try again');
    }
};

/*
* PUT
* Update meditation
* */
export const updateMeditation = async (req, res) => {
    const userId = req.user.id;
    const {id} = req.params;
    try {
        let meditationData = pick(req.body, ['title', 'description', 'categories', 'cover', 'media', 'status', 'meditationType']);
        meditationData.lastUpdatedBy = userId;
        const updatedMeditation = await Meditation.findByIdAndUpdate(id, {...meditationData}, {new: true}).populate({
            path: 'categories',
            select: ['category', 'colour', 'status']
        });
        if (!updatedMeditation) {
            return res.status(404).sendMessage(`Meditation with id ${id} not found!`);
        }
        res.send(updatedMeditation);
    } catch (e) {
        return res.status(400).sendError(e);
    }
};

/*
* DELETE
* Remove meditation data
* */
export const removeMeditation = async (req, res) => {
    const {id} = req.params;
    try {
        await Meditation.findByIdAndDelete(id).then(resp => {
            if (resp) {
                return res.send({messages: `Meditation with id ${id} deleted`});
            }
            return res.status(404).sendError('Meditation not found!');
        });
    } catch (e) {
        return res.status(500).sendError(e);
    }
};

/*
* GET
* Get Meditation By Id
* */
export const getMeditationById = async (req, res) => {
    const {id} = req.params;
    try {
        const meditation = await Meditation.findById(id).populate({
            path: 'categories',
            select: ['category', 'colour', 'status']
        });
        if (meditation) {
            return res.send(meditation);
        }
        return res.status(404).sendError('Meditation not found');
    } catch (e) {
        return res.status(500).sendError(e);
    }
};

/*
* GET
* Get All Meditation
* */
export const getAllMeditation = async (req, res) => {
    try {
        const meditations = await Meditation.find().populate({
            path: 'categories',
            select: ['category', 'colour', 'status']
        });
        res.send(meditations);
    } catch (e) {
        return res.status(500).sendError(e);
    }
};

/*
* GET
* Get All Meditation
* */
export const getAllPublishedMeditation = async (req, res) => {
    try {
        const meditations = await Meditation.find({status: 'PUBLISHED'}).populate({
            path: 'categories',
            select: ['category', 'colour', 'status']
        });
        res.send(meditations);
    } catch (e) {
        return res.status(500).sendError(e);
    }
};

/*
* GET
* Get All Published Meditation By Category
* */
export const getAllPublishedMeditationByCategory = async (req, res) => {
    const {meditationCategory} = req.params;
    try {
        const meditations = await Meditation.find({status: 'PUBLISHED', categories: { "$in" : [meditationCategory]} }).populate({
            path: 'categories',
            select: ['category', 'colour', 'status']
        });
        res.send(meditations);
    } catch (e) {
        return res.status(500).sendError(e);
    }
};
