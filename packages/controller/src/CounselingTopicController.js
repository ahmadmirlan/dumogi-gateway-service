import {CounselingTopic} from '@dumogi/models';
import {pick} from 'lodash';

/*
* POST
* Create new Counseling Topic
* */
export const createNewCounselingTopic = async (req, res) => {
    try {
        let specialist = new CounselingTopic(pick(req.body, ['topic', 'colour', 'status', 'description']));
        specialist = await specialist.save();
        return res.send(specialist);
    } catch (e) {
        return res.status(400).sendError(e);
    }
}

/*
* UPDATE
* Update Counseling Topic
* */
export const updateCounselingTopicById = async (req, res) => {
    const {id} = req.params;
    const specialistData = pick(req.body, ['status', 'topic', 'colour', 'description']);
    try {
        const specialist = await CounselingTopic.findByIdAndUpdate(id, specialistData, {new: true});

        if (!specialist) {
            return res.status(400).sendError('Failed updating specialist');
        }
        return res.send(specialist);
    } catch (e) {
        return res.status(400).sendError(e);
    }
}

/*
* GET
* Find All Counseling Topic
* */
export const getAllCounselingTopic = async (req, res) => {
    try {
        const specialist = await CounselingTopic.find().sort([['createdAt', 'desc']]);
        return res.send(specialist);
    } catch (e) {
        return res.status(400).sendError(e);
    }
}

/*
* GET
* Find Counseling Topic By Id
* */
export const findCounselingTopicById = async (req, res) => {
    const {id} = req.params;
    if (!id) {
        return res.status(400).sendError('Counseling Topic not found');
    }
    try {
        const specialist = await CounselingTopic.findById(id);
        if (!specialist) {
            return res.status(404).sendError('Counseling Topic not found');
        }
        return res.send(specialist);
    } catch (e) {
        return res.status(400).sendError(e);
    }
}

/*
* DELETE
* Delete Counseling Topic By Id
* */
export const deleteCounselingTopicById = async (req, res) => {
    const {id} = req.params;
    if (!id) {
        return res.status(400).sendError('Counseling Topic not found');
    }
    try {
        const specialist = await CounselingTopic.findByIdAndDelete(id);
        if (!specialist) {
            return res.status(404).sendError('Counseling Topic not found');
        }
        return res.send({messages: 'Counseling Topic data deleted!'});
    } catch (e) {
        return res.status(400).sendError(e);
    }
}