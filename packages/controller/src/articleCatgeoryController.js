import {ArticleCategory} from '@dumogi/models';
import {pick} from 'lodash';

/*
* POST
* Create new category
* */
export const createNewCategory = async (req, res) => {
    const {category} = req.body;

    try {
        let categoryData = new ArticleCategory({category});

        categoryData = await categoryData.save();
        return res.send(categoryData);
    } catch (e) {
        res.status(400).sendError(e);
    }
};

/*
* POST
* Find all category
* */
export const findAllCategories = async (req, res) => {
    try {
        const categories = await ArticleCategory.find();
        res.send(categories);
    } catch (e) {
        return res.status(400).sendError(e);
    }
};

/*
* PUT
* Update category
* */
export const updateCategory = async (req, res) => {
    const {id} = req.params;
    const categoryData = pick(req.body, ['status', 'category']);

    try {
        let findCategory = await ArticleCategory.findByIdAndUpdate(id, {...categoryData}, {new: true});

        if (!findCategory) {
            return res.status(404).sendMessage(`Category with id ${id} not found!`);
        }
        res.send(findCategory);
    } catch (e) {
        return res.status(400).sendError(e);
    }
};

/*
* DELETE
* Remove category
* */
export const deleteCategory = async (req, res) => {
    const {id} = req.params;
    try {
        const category = await ArticleCategory.findById(id);
        if (!category) {
            return res.status(404).sendError('Category not found!');
        }
        await ArticleCategory.findByIdAndDelete(id);
        return res.send({messages: `Category with id ${id} deleted!`});
    } catch (e) {
        return res.status(500).sendError('Cannot delete category');
    }
};

/*
* GET
* Find all active category
* */
export const findAllActiveCategory = async (req, res) => {
    try {
        const categories = await ArticleCategory.find({status: 'ACTIVE'});
        return res.send(categories);
    } catch (e) {
        return res.status(500).sendError('Failed load active category');
    }
};
