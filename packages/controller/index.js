import * as authController from './src/AuthController';
import * as userController from './src/UserController';
import * as permissionController from './src/PermissionController';
import * as roleController from './src/RoleController';
import * as articleController from './src/articleController';
import * as articleCategoryController from './src/articleCatgeoryController';
import * as meditationController from './src/meditationController';
import * as meditationCategoryController from './src/meditationCategoryController';
import * as articleCommentController from './src/articleCommentController';
import * as articleCommentReplayController from './src/articleCommentReplayController';
import * as fileController from './src/fileCOntroller';
import * as mantraController from './src/MantraController';
import * as quoteController from './src/QuoteController';
import * as counselingTopicController from './src/CounselingTopicController';
import * as counselorController from './src/CounselorController';

export {
    authController,
    userController,
    permissionController,
    roleController,
    articleController,
    articleCategoryController,
    meditationController,
    meditationCategoryController,
    articleCommentController,
    articleCommentReplayController,
    fileController,
    mantraController,
    quoteController,
    counselingTopicController,
    counselorController
};
